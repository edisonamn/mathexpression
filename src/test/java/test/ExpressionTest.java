package test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.docx4j.Docx4J;
import org.docx4j.math.STJc;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.JcEnumeration;
import org.junit.Assert;
import org.junit.Test;

import com.fabrico.word.math.Expression;

/**
 * @author marcio.napoli@fabrico.com.br
 */
public class ExpressionTest {

	//@formatter:off
	final static String MML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\">\n" +
			"  <semantics>\n" +
			"    <mtable columnalign=\"right left right left right left right left right left right left\" rowspacing=\"3pt\" columnspacing=\"0em 2em 0em 2em 0em 2em 0em 2em 0em 2em 0em\" displaystyle=\"true\">\n" +
			"      <mtr>\n" +
			"        <mtd>\n" +
			"          <mi mathvariant=\"normal\">&#x2207;<!-- ∇ --></mi>\n" +
			"          <mo>&#x00D7;<!-- × --></mo>\n" +
			"          <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"            <mover>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mi mathvariant=\"bold\">B</mi>\n" +
			"              </mrow>\n" +
			"              <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"            </mover>\n" +
			"          </mrow>\n" +
			"          <mo>&#x2212;<!-- − --></mo>\n" +
			"          <mspace width=\"thinmathspace\" />\n" +
			"          <mfrac>\n" +
			"            <mn>1</mn>\n" +
			"            <mi>c</mi>\n" +
			"          </mfrac>\n" +
			"          <mspace width=\"thinmathspace\" />\n" +
			"          <mfrac>\n" +
			"            <mrow>\n" +
			"              <mi mathvariant=\"normal\">&#x2202;<!-- ∂ --></mi>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mover>\n" +
			"                  <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                    <mi mathvariant=\"bold\">E</mi>\n" +
			"                  </mrow>\n" +
			"                  <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"                </mover>\n" +
			"              </mrow>\n" +
			"            </mrow>\n" +
			"            <mrow>\n" +
			"              <mi mathvariant=\"normal\">&#x2202;<!-- ∂ --></mi>\n" +
			"              <mi>t</mi>\n" +
			"            </mrow>\n" +
			"          </mfrac>\n" +
			"        </mtd>\n" +
			"        <mtd>\n" +
			"          <mi></mi>\n" +
			"          <mo>=</mo>\n" +
			"          <mfrac>\n" +
			"            <mrow>\n" +
			"              <mn>4</mn>\n" +
			"              <mi>&#x03C0;<!-- π --></mi>\n" +
			"            </mrow>\n" +
			"            <mi>c</mi>\n" +
			"          </mfrac>\n" +
			"          <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"            <mover>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mi mathvariant=\"bold\">j</mi>\n" +
			"              </mrow>\n" +
			"              <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"            </mover>\n" +
			"          </mrow>\n" +
			"        </mtd>\n" +
			"      </mtr>\n" +
			"      <mtr>\n" +
			"        <mtd>\n" +
			"          <mi mathvariant=\"normal\">&#x2207;<!-- ∇ --></mi>\n" +
			"          <mo>&#x22C5;<!-- ⋅ --></mo>\n" +
			"          <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"            <mover>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mi mathvariant=\"bold\">E</mi>\n" +
			"              </mrow>\n" +
			"              <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"            </mover>\n" +
			"          </mrow>\n" +
			"        </mtd>\n" +
			"        <mtd>\n" +
			"          <mi></mi>\n" +
			"          <mo>=</mo>\n" +
			"          <mn>4</mn>\n" +
			"          <mi>&#x03C0;<!-- π --></mi>\n" +
			"          <mi>&#x03C1;<!-- ρ --></mi>\n" +
			"        </mtd>\n" +
			"      </mtr>\n" +
			"      <mtr>\n" +
			"        <mtd>\n" +
			"          <mi mathvariant=\"normal\">&#x2207;<!-- ∇ --></mi>\n" +
			"          <mo>&#x00D7;<!-- × --></mo>\n" +
			"          <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"            <mover>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mi mathvariant=\"bold\">E</mi>\n" +
			"              </mrow>\n" +
			"              <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"            </mover>\n" +
			"          </mrow>\n" +
			"          <mspace width=\"thinmathspace\" />\n" +
			"          <mo>+</mo>\n" +
			"          <mspace width=\"thinmathspace\" />\n" +
			"          <mfrac>\n" +
			"            <mn>1</mn>\n" +
			"            <mi>c</mi>\n" +
			"          </mfrac>\n" +
			"          <mspace width=\"thinmathspace\" />\n" +
			"          <mfrac>\n" +
			"            <mrow>\n" +
			"              <mi mathvariant=\"normal\">&#x2202;<!-- ∂ --></mi>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mover>\n" +
			"                  <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                    <mi mathvariant=\"bold\">B</mi>\n" +
			"                  </mrow>\n" +
			"                  <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"                </mover>\n" +
			"              </mrow>\n" +
			"            </mrow>\n" +
			"            <mrow>\n" +
			"              <mi mathvariant=\"normal\">&#x2202;<!-- ∂ --></mi>\n" +
			"              <mi>t</mi>\n" +
			"            </mrow>\n" +
			"          </mfrac>\n" +
			"        </mtd>\n" +
			"        <mtd>\n" +
			"          <mi></mi>\n" +
			"          <mo>=</mo>\n" +
			"          <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"            <mover>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mn mathvariant=\"bold\">0</mn>\n" +
			"              </mrow>\n" +
			"              <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"            </mover>\n" +
			"          </mrow>\n" +
			"        </mtd>\n" +
			"      </mtr>\n" +
			"      <mtr>\n" +
			"        <mtd>\n" +
			"          <mi mathvariant=\"normal\">&#x2207;<!-- ∇ --></mi>\n" +
			"          <mo>&#x22C5;<!-- ⋅ --></mo>\n" +
			"          <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"            <mover>\n" +
			"              <mrow class=\"MJX-TeXAtom-ORD\">\n" +
			"                <mi mathvariant=\"bold\">B</mi>\n" +
			"              </mrow>\n" +
			"              <mo stretchy=\"false\">&#x2192;<!-- → --></mo>\n" +
			"            </mover>\n" +
			"          </mrow>\n" +
			"        </mtd>\n" +
			"        <mtd>\n" +
			"          <mi></mi>\n" +
			"          <mo>=</mo>\n" +
			"          <mn>0</mn>\n" +
			"        </mtd>\n" +
			"      </mtr>\n" +
			"    </mtable>\n" +
			"    <annotation encoding=\"application/x-tex\">\\begin{align}\n" +
			"  \\nabla \\times \\vec{\\mathbf{B}} -\\, \\frac1c\\, \\frac{\\partial\\vec{\\mathbf{E}}}{\\partial t} &amp; = \\frac{4\\pi}{c}\\vec{\\mathbf{j}} \\\\\n" +
			"  \\nabla \\cdot \\vec{\\mathbf{E}} &amp; = 4 \\pi \\rho \\\\\n" +
			"  \\nabla \\times \\vec{\\mathbf{E}}\\, +\\, \\frac1c\\, \\frac{\\partial\\vec{\\mathbf{B}}}{\\partial t} &amp; = \\vec{\\mathbf{0}} \\\\\n" +
			"  \\nabla \\cdot \\vec{\\mathbf{B}} &amp; = 0\n" +
			"\\end{align}</annotation>\n" +
			"  </semantics>\n" +
			"</math>";
	//@formatter:on

	//@formatter:off
	final static String OMML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<m:oMath xmlns:mml=\"http://www.w3.org/1998/Math/MathML\"\n" +
			"         xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\">\n" +
			"   <m:m>\n" +
			"      <m:mPr>\n" +
			"         <m:baseJc m:val=\"left\"/>\n" +
			"         <m:plcHide m:val=\"on\"/>\n" +
			"         <m:mcs>\n" +
			"            <m:mc>\n" +
			"               <m:mcPr>\n" +
			"                  <m:count m:val=\"2\"/>\n" +
			"                  <m:mcJc m:val=\"center\"/>\n" +
			"               </m:mcPr>\n" +
			"            </m:mc>\n" +
			"         </m:mcs>\n" +
			"      </m:mPr>\n" +
			"      <m:mr>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:rPr>\n" +
			"                  <m:sty m:val=\"p\"/>\n" +
			"               </m:rPr>\n" +
			"               <m:t>∇</m:t>\n" +
			"            </m:r>\n" +
			"            <m:r>\n" +
			"               <m:t>×</m:t>\n" +
			"            </m:r>\n" +
			"            <m:limUpp>\n" +
			"               <m:e>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"b\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>B</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:e>\n" +
			"               <m:lim>\n" +
			"                  <m:r>\n" +
			"                     <m:t>→</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:lim>\n" +
			"            </m:limUpp>\n" +
			"            <m:r>\n" +
			"               <m:t>−</m:t>\n" +
			"            </m:r>\n" +
			"            <m:f>\n" +
			"               <m:fPr>\n" +
			"                  <m:type m:val=\"bar\"/>\n" +
			"               </m:fPr>\n" +
			"               <m:num>\n" +
			"                  <m:r>\n" +
			"                     <m:t>1</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:num>\n" +
			"               <m:den>\n" +
			"                  <m:r>\n" +
			"                     <m:t>c</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:den>\n" +
			"            </m:f>\n" +
			"            <m:f>\n" +
			"               <m:fPr>\n" +
			"                  <m:type m:val=\"bar\"/>\n" +
			"               </m:fPr>\n" +
			"               <m:num>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"p\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>∂</m:t>\n" +
			"                  </m:r>\n" +
			"                  <m:limUpp>\n" +
			"                     <m:e>\n" +
			"                        <m:r>\n" +
			"                           <m:rPr>\n" +
			"                              <m:sty m:val=\"b\"/>\n" +
			"                           </m:rPr>\n" +
			"                           <m:t>E</m:t>\n" +
			"                        </m:r>\n" +
			"                     </m:e>\n" +
			"                     <m:lim>\n" +
			"                        <m:r>\n" +
			"                           <m:t>→</m:t>\n" +
			"                        </m:r>\n" +
			"                     </m:lim>\n" +
			"                  </m:limUpp>\n" +
			"               </m:num>\n" +
			"               <m:den>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"p\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>∂</m:t>\n" +
			"                  </m:r>\n" +
			"                  <m:r>\n" +
			"                     <m:t>t</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:den>\n" +
			"            </m:f>\n" +
			"         </m:e>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:t>=</m:t>\n" +
			"            </m:r>\n" +
			"            <m:f>\n" +
			"               <m:fPr>\n" +
			"                  <m:type m:val=\"bar\"/>\n" +
			"               </m:fPr>\n" +
			"               <m:num>\n" +
			"                  <m:r>\n" +
			"                     <m:t>4π</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:num>\n" +
			"               <m:den>\n" +
			"                  <m:r>\n" +
			"                     <m:t>c</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:den>\n" +
			"            </m:f>\n" +
			"            <m:limUpp>\n" +
			"               <m:e>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"b\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>j</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:e>\n" +
			"               <m:lim>\n" +
			"                  <m:r>\n" +
			"                     <m:t>→</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:lim>\n" +
			"            </m:limUpp>\n" +
			"         </m:e>\n" +
			"      </m:mr>\n" +
			"      <m:mr>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:rPr>\n" +
			"                  <m:sty m:val=\"p\"/>\n" +
			"               </m:rPr>\n" +
			"               <m:t>∇</m:t>\n" +
			"            </m:r>\n" +
			"            <m:r>\n" +
			"               <m:t>⋅</m:t>\n" +
			"            </m:r>\n" +
			"            <m:limUpp>\n" +
			"               <m:e>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"b\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>E</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:e>\n" +
			"               <m:lim>\n" +
			"                  <m:r>\n" +
			"                     <m:t>→</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:lim>\n" +
			"            </m:limUpp>\n" +
			"         </m:e>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:t>=4πρ</m:t>\n" +
			"            </m:r>\n" +
			"         </m:e>\n" +
			"      </m:mr>\n" +
			"      <m:mr>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:rPr>\n" +
			"                  <m:sty m:val=\"p\"/>\n" +
			"               </m:rPr>\n" +
			"               <m:t>∇</m:t>\n" +
			"            </m:r>\n" +
			"            <m:r>\n" +
			"               <m:t>×</m:t>\n" +
			"            </m:r>\n" +
			"            <m:limUpp>\n" +
			"               <m:e>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"b\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>E</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:e>\n" +
			"               <m:lim>\n" +
			"                  <m:r>\n" +
			"                     <m:t>→</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:lim>\n" +
			"            </m:limUpp>\n" +
			"            <m:r>\n" +
			"               <m:t>+</m:t>\n" +
			"            </m:r>\n" +
			"            <m:f>\n" +
			"               <m:fPr>\n" +
			"                  <m:type m:val=\"bar\"/>\n" +
			"               </m:fPr>\n" +
			"               <m:num>\n" +
			"                  <m:r>\n" +
			"                     <m:t>1</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:num>\n" +
			"               <m:den>\n" +
			"                  <m:r>\n" +
			"                     <m:t>c</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:den>\n" +
			"            </m:f>\n" +
			"            <m:f>\n" +
			"               <m:fPr>\n" +
			"                  <m:type m:val=\"bar\"/>\n" +
			"               </m:fPr>\n" +
			"               <m:num>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"p\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>∂</m:t>\n" +
			"                  </m:r>\n" +
			"                  <m:limUpp>\n" +
			"                     <m:e>\n" +
			"                        <m:r>\n" +
			"                           <m:rPr>\n" +
			"                              <m:sty m:val=\"b\"/>\n" +
			"                           </m:rPr>\n" +
			"                           <m:t>B</m:t>\n" +
			"                        </m:r>\n" +
			"                     </m:e>\n" +
			"                     <m:lim>\n" +
			"                        <m:r>\n" +
			"                           <m:t>→</m:t>\n" +
			"                        </m:r>\n" +
			"                     </m:lim>\n" +
			"                  </m:limUpp>\n" +
			"               </m:num>\n" +
			"               <m:den>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"p\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>∂</m:t>\n" +
			"                  </m:r>\n" +
			"                  <m:r>\n" +
			"                     <m:t>t</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:den>\n" +
			"            </m:f>\n" +
			"         </m:e>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:t>=</m:t>\n" +
			"            </m:r>\n" +
			"            <m:limUpp>\n" +
			"               <m:e>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"b\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>0</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:e>\n" +
			"               <m:lim>\n" +
			"                  <m:r>\n" +
			"                     <m:t>→</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:lim>\n" +
			"            </m:limUpp>\n" +
			"         </m:e>\n" +
			"      </m:mr>\n" +
			"      <m:mr>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:rPr>\n" +
			"                  <m:sty m:val=\"p\"/>\n" +
			"               </m:rPr>\n" +
			"               <m:t>∇</m:t>\n" +
			"            </m:r>\n" +
			"            <m:r>\n" +
			"               <m:t>⋅</m:t>\n" +
			"            </m:r>\n" +
			"            <m:limUpp>\n" +
			"               <m:e>\n" +
			"                  <m:r>\n" +
			"                     <m:rPr>\n" +
			"                        <m:sty m:val=\"b\"/>\n" +
			"                     </m:rPr>\n" +
			"                     <m:t>B</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:e>\n" +
			"               <m:lim>\n" +
			"                  <m:r>\n" +
			"                     <m:t>→</m:t>\n" +
			"                  </m:r>\n" +
			"               </m:lim>\n" +
			"            </m:limUpp>\n" +
			"         </m:e>\n" +
			"         <m:e>\n" +
			"            <m:r>\n" +
			"               <m:t>=0</m:t>\n" +
			"            </m:r>\n" +
			"         </m:e>\n" +
			"      </m:mr>\n" +
			"   </m:m>\n" +
			"</m:oMath>\n";
	//@formatter:on

	@Test
	public void convertMml2Omml() throws Exception {
		final String omml = Expression.getInstance().mml2omml(MML);
		Assert.assertEquals(OMML, omml);
	}

	@Test
	public void convert2Omml() throws Exception {
		final String omml = Expression.getInstance().mml2omml(MML);
		final WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
		final MainDocumentPart document = wordMLPackage.getMainDocumentPart();
		Expression.getInstance().addOmmlToDocx(omml, document);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		wordMLPackage.save(baos, Docx4J.FLAG_SAVE_FLAT_XML);
		Assert.assertTrue(baos.toByteArray().length > 0);
	}

	@Test
	public void convert2OmmlToFile() throws Exception {
		final String omml = Expression.getInstance().mml2omml(MML);
		final WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
		final MainDocumentPart document = wordMLPackage.getMainDocumentPart();
		Expression.getInstance().addOmmlToDocx(omml, document, STJc.LEFT);
		final FileOutputStream baos = new FileOutputStream(new File("teste.odt"));
		wordMLPackage.save(baos, Docx4J.FLAG_SAVE_ZIP_FILE);
	}
	
	
	@Test
	public void convert2OmmlToDoc() throws Exception {
		final String omml = Expression.getInstance().mml2omml(MML);
		final WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
		final MainDocumentPart document = wordMLPackage.getMainDocumentPart();
		Expression.getInstance().addOmmlToDocx(omml, document, STJc.LEFT);
		final FileOutputStream baos = new FileOutputStream(new File("teste.docx"));
		wordMLPackage.save(baos, Docx4J.FLAG_SAVE_ZIP_FILE);
		Expression.getInstance().convertFromOdtToDoc("teste.docx", "/opt/libreofficedev5.1/program");
	}
	
	
}
