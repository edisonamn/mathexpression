package com.fabrico.word.math;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.math.CTCtrlPr;
import org.docx4j.math.CTF;
import org.docx4j.math.CTFPr;
import org.docx4j.math.CTLimUpp;
import org.docx4j.math.CTLimUppPr;
import org.docx4j.math.CTM;
import org.docx4j.math.CTMPr;
import org.docx4j.math.CTMR;
import org.docx4j.math.CTMathPr;
import org.docx4j.math.CTOMath;
import org.docx4j.math.CTOMathArg;
import org.docx4j.math.CTOMathJc;
import org.docx4j.math.CTOMathPara;
import org.docx4j.math.CTOMathParaPr;
import org.docx4j.math.CTOnOff;
import org.docx4j.math.CTR;
import org.docx4j.math.CTR.RPrMath;
import org.docx4j.math.CTRPR;
import org.docx4j.math.CTString;
import org.docx4j.math.STJc;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.WordprocessingML.DocumentSettingsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.FontTablePart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.WordprocessingML.StyleDefinitionsPart;
import org.docx4j.sharedtypes.STOnOff;
import org.docx4j.wml.CTSettings;
import org.docx4j.wml.CTUcharHexNumber;
import org.docx4j.wml.FontFamily;
import org.docx4j.wml.FontPitch;
import org.docx4j.wml.Fonts;
import org.docx4j.wml.Fonts.Font;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.ParaRPr;
import org.docx4j.wml.RFonts;
import org.docx4j.wml.RPr;
import org.docx4j.wml.STPitch;
import org.docx4j.wml.Style;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import net.sf.saxon.jaxp.SaxonTransformerFactory;

/**
 * @author marcio.napoli@fabrico.com.br
 */
public class Expression {

	static final String RESOURCE_MML2OMML = "MML2OMML.XSL";

	static ObjectFactory FACTORY = Context.getWmlObjectFactory();
	static org.docx4j.math.ObjectFactory mathFactory = new org.docx4j.math.ObjectFactory();

	private static volatile Transformer transformer = null;

	private Expression() {
		super();
	}

	/**
	 * Converts an MML (Math Markup Language) to OMML (Office Math Markup
	 * Language).
	 *
	 * @param mml
	 * @return omml
	 * @throws Exception
	 */
	public synchronized String mml2omml(String mml) throws Exception {
		try {
			if (transformer == null) {
				final InputStream template = Expression.class.getClassLoader().getResourceAsStream(RESOURCE_MML2OMML);
				transformer = new SaxonTransformerFactory().newTransformer(new StreamSource(template));
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			}

			final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(new ByteArrayInputStream(mml.getBytes(UTF_8)));
			final DOMSource ds = new DOMSource(document);
			final StringWriter writer = new StringWriter();
			final StreamResult sr = new StreamResult(writer);
			transformer.transform(ds, sr);
			return writer.toString();
		} catch (final TransformerConfigurationException e) {
			throw new Exception("Problema na configuração do XSLT", e);
		} catch (final SAXException e) {
			throw new Exception("Problema ao realizar o parser do XML", e);
		} catch (final IOException e) {
			throw new Exception("Problema ao ler/escrever conteúdo de entrada/saída", e);
		} catch (final ParserConfigurationException e) {
			throw new Exception("Configuração do parser está incorreta", e);
		} catch (final TransformerException e) {
			throw new Exception("Problema ao realizar o processamento do XSLT", e);
		}
	}

	/**
	 * Add the expression in the document (docx4j). In this case a paragraph (p)
	 * is created to insert the expression.
	 *
	 * @param omml
	 * @param document
	 * @throws Exception
	 */
	public void addOmmlToDocx(String omml, MainDocumentPart document) throws Exception {
		this.addOmmlToDocx(omml, document, null);
	}

	/**
	 * Add the expression in the document (docx4j). In this case a paragraph (p)
	 * is created to insert the expression.
	 *
	 * @param omml
	 * @param document
	 * @param textAlignment
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void addOmmlToDocx(String omml, MainDocumentPart document, STJc mathAlignment) throws Exception {
		try {
			final JAXBElement<CTOMath> unmarshal = (JAXBElement<CTOMath>) XmlUtils.unmarshalString(omml);
			this.addOmmlToDocx(unmarshal, document, mathAlignment);
		} catch (final JAXBException e) {
			throw new Exception("Não foi possível realizar o parser do XML", e);
		}

	}
	
	public void convertFromOdtToDoc(String odtFile, String libreOfficeLocation) throws IOException{
		Runtime rt = Runtime.getRuntime();
		StringBuilder command = new StringBuilder();
//		command.append("cd ");
//		command.append(libreOfficeLocation);
//		command.append("; ");
		command.append("soffice --convert-to doc --outdir /tmp ");
		command.append(odtFile);
		Process process = rt.exec(command.toString());
		InputStream inputStream = process.getInputStream();
		int read = 0;
		while (read != -1){
			read = inputStream.read();
			System.out.println(read);
		}
	}

	/**
	 * Add the expression in the document (docx4j). In this case a paragraph (p)
	 * is created to insert the expression.
	 *
	 * @param omml
	 * @param document
	 * @throws Exception
	 */
	public void addOmmlToDocx(JAXBElement<CTOMath> omml, MainDocumentPart document, STJc mathAlignment) throws Exception {
		if (mathAlignment == null) {
			mathAlignment = STJc.LEFT;
		}
		
		CTRPR rpr = new CTRPR();
		CTOnOff normalText = new CTOnOff();
		normalText.setVal(STOnOff.ON);
		rpr.setNor(normalText);
		
		updateOmmlStyle(omml);
		P p = createMathParagraph(omml, mathAlignment);
		document.getContent().add(p);
		
		DocumentSettingsPart mainPart = createMathSettings();
		document.addTargetPart(mainPart);

		StyleDefinitionsPart styleDefinitions = document.getStyleDefinitionsPart();
		Style charStyle = styleDefinitions.getDefaultCharacterStyle();
		Style paragraphStyle = styleDefinitions.getDefaultParagraphStyle();
		Style tableStyle = styleDefinitions.getDefaultTableStyle();
		charStyle.setRPr(createFontDefinitions());
		paragraphStyle.setRPr(createFontDefinitions());
		tableStyle.setRPr(createFontDefinitions());
		
		FontTablePart fontsPart = createFontTable();
		document.addTargetPart(fontsPart);
		
		
	}

	/**
	 * This method updates the CTOMatch and insert the font in the package.
	 * 
	 * @param omml
	 */
	@SuppressWarnings("unchecked")
	private void updateOmmlStyle(JAXBElement<CTOMath> omml) {
		CTOMath ctoMath = omml.getValue();
		List<Object> egoMathElements = ctoMath.getEGOMathElements();
		for (Object object : egoMathElements) {
			JAXBElement<Object> element = (JAXBElement<Object>) object;
			if (element == null){
				continue;
			}
			CTM ctm = (CTM) element.getValue();
			if (ctm == null) {
				continue;
			}
			CTMPr mPr = ctm.getMPr();
			if (mPr == null) {
				mPr = mathFactory.createCTMPr();
				ctm.setMPr(mPr);
			}
			
			mPr.setCtrlPr(createCTCrlPrWithPatternFont());
			List<CTMR> mrList = ctm.getMr();
			if (mrList == null || mrList.isEmpty()) {
				continue;
			}
			for (CTMR mr : mrList) {
				List<CTOMathArg> arguments = mr.getE();
				if (arguments == null || arguments.isEmpty()){
					continue;
				}
				for (CTOMathArg argument : arguments) {
					argument.setCtrlPr(createCTCrlPrWithPatternFont());
					createFontToEgoMathElements(argument.getEGOMathElements());
					
				}
			}
		}
	}

	/**
	 * Iterates the List and put the font inside of each element.
	 * 
	 * @param ctrlPr
	 * @param argument
	 */
	private void createFontToEgoMathElements(List<Object> egoMathElements) {
		
		CTCtrlPr ctrlPr = createCTCrlPrWithPatternFont();
		
		for (Object mathArg : egoMathElements) {
			Object argumentObject = ((JAXBElement<Object>) mathArg).getValue();
			if (argumentObject instanceof CTLimUpp) {
				createFontToCTLimUpp((CTLimUpp)argumentObject);
				continue;
			}
			
			if (argumentObject instanceof CTF) {
				createFontCTF((CTF)argumentObject);
				continue;
			}
			createFontToCTR((CTR) argumentObject);
		}
	}

	/**
	 * Creates the font and norma text parameter inside <code>CTF</code> tree.
	 * 
	 * @param ctf
	 */
	private void createFontCTF(CTF ctf) {
		CTCtrlPr ctrlPr = createCTCrlPrWithPatternFont();
		CTFPr fpr = mathFactory.createCTFPr();
		fpr.setCtrlPr(ctrlPr);

		ctf.setFPr(fpr);
		CTOMathArg den = ctf.getDen();
		if (den != null) {					
			den.setCtrlPr(ctrlPr);
			createFontToEgoMathElements(den.getEGOMathElements());
		}
		CTOMathArg num = ctf.getNum();
		if (num != null){
			num.setCtrlPr(ctrlPr);
			createFontToEgoMathElements(num.getEGOMathElements());
		}
	}

	/**
	 * Creates the font and norma text parameter inside <code>CTLimUpp</code> tree.
	 * 
	 * @param ctLimUpp
	 */
	private void createFontToCTLimUpp(CTLimUpp ctLimUpp) {
		CTCtrlPr ctrlPr = createCTCrlPrWithPatternFont();
		CTLimUppPr limUppPr = mathFactory.createCTLimUppPr();
		limUppPr.setCtrlPr(ctrlPr);
		
		ctLimUpp.setLimUppPr(limUppPr);
		CTOMathArg e = ctLimUpp.getE();
		if (e != null){
			e.setCtrlPr(ctrlPr);
			createFontToEgoMathElements(e.getEGOMathElements());
		}
		CTOMathArg lim = ctLimUpp.getLim();
		if (lim != null) {
			lim.setCtrlPr(ctrlPr);
			createFontToEgoMathElements(lim.getEGOMathElements());
		}
	}
	
	private void createFontToCTR(CTR ctr){
		CTRPR ctrpr = new CTRPR();
		CTOnOff onOff = mathFactory.createCTOnOff();
		onOff.setVal(STOnOff.TRUE);
		ctrpr.setNor(onOff);
		RPrMath rprMath = mathFactory.createCTRRPrMath(ctrpr);
		
		if (ctr.getContent().size() > 1) {
			for (Object ctrContent : ctr.getContent()) {
				if (ctrContent instanceof RPrMath) {
					((RPrMath) ctrContent).setValue(ctrpr);
				}
			}
			ctr.getContent().add(1, createFontDefinitions());
			return;
		} 
		ctr.getContent().add(0, rprMath);
		ctr.getContent().add(1, createFontDefinitions());
	}

	private CTCtrlPr createCTCrlPrWithPatternFont() {
		CTCtrlPr ctrlPr = new CTCtrlPr();
		ctrlPr.setRPr(createFontDefinitions());
		return ctrlPr;
	}

	/**
	 * This method creates the pattern settings file to the document.
	 * @return
	 * @throws InvalidFormatException
	 */
	private DocumentSettingsPart createMathSettings() throws InvalidFormatException {
		CTString ctString = mathFactory.createCTString();
		ctString.setVal("Times New Roman");
	
		CTMathPr ctMathPr = mathFactory.createCTMathPr();
		ctMathPr.setMathFont(ctString);
		
		CTSettings settings = FACTORY.createCTSettings();
		settings.setMathPr(ctMathPr);
		
		DocumentSettingsPart mainPart = new DocumentSettingsPart();
		mainPart.setJaxbElement(settings);
		return mainPart;
	}

	private RPr createFontDefinitions() {
		final RFonts runFonts = createRFont();
		
		HpsMeasure size = createHpsMeasure();
		
		RPr rpr = FACTORY.createRPr();
		rpr.setRFonts(runFonts);
		rpr.setSz(size);
		return rpr;
	}

	private HpsMeasure createHpsMeasure() {
		HpsMeasure size = new HpsMeasure();
		size.setVal(BigInteger.valueOf(24));
		return size;
	}

	private RFonts createRFont() {
		RFonts runFonts = new RFonts();
		runFonts.setAscii("Times New Roman");
		runFonts.setHAnsi("Cambria Math");
		runFonts.setCs("Times New Roman");
		return runFonts;
	}

	private FontTablePart createFontTable() throws InvalidFormatException {
		FontTablePart fontsPart = new FontTablePart();
		Fonts fonts = FACTORY.createFonts();
		List<Font> fontList = fonts.getFont();
		Font font = new Font();
		font.setName("Times New Roman");
		CTUcharHexNumber charHexNumber = FACTORY.createCTUcharHexNumber();
		charHexNumber.setVal("00");
		font.setCharset(charHexNumber);
		
		FontFamily fontFamily = FACTORY.createFontFamily();
		fontFamily.setVal("roman");
		font.setFamily(fontFamily);
		
		FontPitch fontPitch = FACTORY.createFontPitch();
		fontPitch.setVal(STPitch.VARIABLE);
		font.setPitch(fontPitch);
		
		fontList.add(font);
		fontsPart.setJaxbElement(fonts);
		return fontsPart;
	}
	
	/**
	 * Creates the Math pattern paragraph 
	 * 
	 * @param omml
	 * @param mathAlignment
	 * @return
	 */
	private P createMathParagraph(JAXBElement<CTOMath> omml, STJc mathAlignment){
		
		CTOMathJc oMathJc = mathFactory.createCTOMathJc();
		oMathJc.setVal(mathAlignment);
		
		CTOMathParaPr oMathPpr = mathFactory.createCTOMathParaPr();
		oMathPpr.setJc(oMathJc);
		
		CTOMathPara oMathParagraph = mathFactory.createCTOMathPara();
		List<CTOMath> oMath = oMathParagraph.getOMath();
		oMath.add(omml.getValue());
		oMathParagraph.setOMathParaPr(oMathPpr);
		JAXBElement<CTOMathPara> oMathParaElement = mathFactory.createOMathPara(oMathParagraph);
		
		ParaRPr paraRpr = FACTORY.createParaRPr();
		paraRpr.setRFonts(createRFont());
		paraRpr.setSz(createHpsMeasure());
		
		PPr ppr = FACTORY.createPPr();
		ppr.setRPr(paraRpr);
		
		final P p = FACTORY.createP();
		p.setPPr(ppr);
		p.getContent().add(oMathParaElement);
		
		return p;
	}

	/**
	 * Add the expression in the document (docx4j). In this case a paragraph (p)
	 * is created to insert the expression.
	 *
	 * @param mml
	 * @param document
	 * @throws Exception
	 */
	public void addMmlToDocx(String mml, MainDocumentPart document) throws Exception {
		this.addOmmlToDocx(this.mml2omml(mml), document);
	}

	private static class TransformerHolder {
		public static Expression INSTANCE = new Expression();
	}

	public static Expression getInstance() {
		return TransformerHolder.INSTANCE;
	}

}
